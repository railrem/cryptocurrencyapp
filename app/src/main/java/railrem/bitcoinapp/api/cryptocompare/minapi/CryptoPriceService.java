package railrem.bitcoinapp.api.cryptocompare.minapi;


import com.google.gson.JsonObject;

import java.util.HashMap;

import railrem.bitcoinapp.models.CryptoPriceBox;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Timur Valiev
 */
public interface CryptoPriceService {

    @GET("price")
    Call<HashMap<String, Double>> price(@Query("fsym") String fsym);

    @GET("histoday")
    Call<CryptoPriceBox> historyPrice(@Query("fsym") String fsym, @Query("limit") Integer limit ,@Query("tsym") String tsym );

    @GET("price")
    Call<JsonObject> price(@Query("fsym") String fsym, @Query("tsyms") String tsyms);

//    https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=BTC,USD,EUR
}
