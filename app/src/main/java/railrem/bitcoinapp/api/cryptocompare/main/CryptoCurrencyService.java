package railrem.bitcoinapp.api.cryptocompare.main;


import com.google.gson.JsonObject;

import railrem.bitcoinapp.models.CryptoCurrencyBox;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Timur Valiev
 */
public interface CryptoCurrencyService {

    @GET("coinlist")
    Call<CryptoCurrencyBox> getCoinList();

    @GET("coinsnapshotfullbyid")
    Call<JsonObject> getCurrencyInfo(@Query("id") Integer id);
}
