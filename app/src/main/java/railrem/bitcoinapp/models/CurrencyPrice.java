package railrem.bitcoinapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by railr on 24.08.2017.
 */

public class CurrencyPrice {

    @SerializedName("time")
    private Long time;
    @SerializedName("close")
    private Double close;
    @SerializedName("high")
    private Double high;
    @SerializedName("low")
    private Double low;
    @SerializedName("open")
    private Double open;
    @SerializedName("volumefrom")
    private Double volumefrom;
    @SerializedName("volumeto")
    private Double volumeto;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getVolumefrom() {
        return volumefrom;
    }

    public void setVolumefrom(Double volumefrom) {
        this.volumefrom = volumefrom;
    }

    public Double getVolumeto() {
        return volumeto;
    }

    public void setVolumeto(Double volumeto) {
        this.volumeto = volumeto;
    }
}

