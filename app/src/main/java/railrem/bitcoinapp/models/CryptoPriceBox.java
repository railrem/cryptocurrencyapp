package railrem.bitcoinapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by railr on 24.08.2017.
 */

public class CryptoPriceBox {

    @SerializedName("Response")
    private String response;
    @SerializedName("Type")
    private Long type;
    @SerializedName("Aggregated")
    private Boolean aggregated;
    @SerializedName("TimeTo")
    private Long timeTo;
    @SerializedName("TimeFrom")
    private Long timeFrom;
    @SerializedName("FirstValueInArray")
    private Boolean firstValueInArray;

    @SerializedName("Data")
    private ArrayList<CurrencyPrice> data;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Boolean getAggregated() {
        return aggregated;
    }

    public void setAggregated(Boolean aggregated) {
        this.aggregated = aggregated;
    }

    public Long getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(Long timeTo) {
        this.timeTo = timeTo;
    }

    public Long getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(Long timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Boolean getFirstValueInArray() {
        return firstValueInArray;
    }

    public void setFirstValueInArray(Boolean firstValueInArray) {
        this.firstValueInArray = firstValueInArray;
    }

    public ArrayList<CurrencyPrice> getData() {
        return data;
    }

    public void setData(ArrayList<CurrencyPrice> data) {
        this.data = data;
    }
}
