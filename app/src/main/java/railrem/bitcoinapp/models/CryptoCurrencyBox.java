package railrem.bitcoinapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by railr on 23.08.2017.
 */

public class CryptoCurrencyBox {

    @SerializedName("Response")
    private String response;

    @SerializedName("Message")
    private String message;

    @SerializedName("BaseImageUrl")
    private String baseImageUrl;

    @SerializedName("BaseLinkUrl")
    private String baseLinkUrl;

    @SerializedName("Data")
    private HashMap<String,CryptoCurrency> data;

    @SerializedName("Type")
    private Long type;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBaseImageUrl() {
        return baseImageUrl;
    }

    public void setBaseImageUrl(String baseImageUrl) {
        this.baseImageUrl = baseImageUrl;
    }

    public String getBaseLinkUrl() {
        return baseLinkUrl;
    }

    public void setBaseLinkUrl(String baseLinkUrl) {
        this.baseLinkUrl = baseLinkUrl;
    }


    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public HashMap<String, CryptoCurrency> getData() {
        return data;
    }

    public void setData(HashMap<String, CryptoCurrency> data) {
        this.data = data;
    }
}
