package railrem.bitcoinapp.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import railrem.bitcoinapp.R;
import railrem.bitcoinapp.api.cryptocompare.main.CryptoCurrencyService;
import railrem.bitcoinapp.api.cryptocompare.main.MainApiFactory;
import railrem.bitcoinapp.api.cryptocompare.minapi.CryptoPriceService;
import railrem.bitcoinapp.api.cryptocompare.minapi.MinApiFactory;
import railrem.bitcoinapp.models.CryptoPriceBox;
import railrem.bitcoinapp.models.CurrencyPrice;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CurrencyActivity extends AppCompatActivity {

    public static final String EXTRA_CRYPTO_NAME = "extra_crypto_name";
    public static final String EXTRA_CRYPTO_ID = "extra_crypto_id";


    @BindView(R.id.priceChart)
    LineChart priceChart;

    @BindView(R.id.short_name)
    TextView shortName;

    @BindView(R.id.full_name)
    TextView fullName;

    @BindView(R.id.price)
    TextView price;

    @BindView(R.id.icon)
    ImageView icon;

    @BindView(R.id.targetCurrencies)
    Spinner targetCurrencies;

    String[] items;

    @OnClick(R.id.refresh)
    public void refreshPrice() {
        loadingDialog.show();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        fetchPrice(shortName.getText().toString(), items[targetCurrencies.getSelectedItemPosition()]);
    }

    @OnItemSelected(R.id.targetCurrencies)
    public void updatePrices() {
        loadingDialog.show();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        fetchPrice(shortName.getText().toString(), items[targetCurrencies.getSelectedItemPosition()]);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        fetchHistoricPrice(shortName.getText().toString(), items[targetCurrencies.getSelectedItemPosition()]);
    }

    private ProgressDialog loadingDialog;

    private CryptoCurrencyService cryptoCurrencyService;
    private CryptoPriceService cryptoPriceService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_currency);
        ButterKnife.bind(this);
        String currency = getIntent().getStringExtra(EXTRA_CRYPTO_NAME);
        String currencyId = getIntent().getStringExtra(EXTRA_CRYPTO_ID);
        items = getResources().getStringArray(R.array.tsyms);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.tsyms, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        targetCurrencies.setAdapter(adapter);
        loadingDialog = ProgressDialog.show(this, "", getString(R.string.network_loading), true);
        cryptoCurrencyService = MainApiFactory.getRetrofitInstance().create(CryptoCurrencyService.class);
        cryptoPriceService = MinApiFactory.getRetrofitInstance().create(CryptoPriceService.class);
        fetchCurrencyInfo(new Integer(currencyId));
        // !!! Оказывается у апи есть ограничение не чаще 1 запроса в секунду!! ((((((
        loadingDialog.show();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String resultCurrency = getString(R.string.def_currency);
        fetchPrice(currency, resultCurrency);
        loadingDialog.show();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        fetchHistoricPrice(currency, resultCurrency);
        prepareChart();
    }


    private void fetchCurrencyInfo(Integer id) {
        // Создаем экземпляр запроса со всем необходимыми настройками
        Call<JsonObject> call = cryptoCurrencyService.getCurrencyInfo(id);

        // Выполняем запрос асинхронно
        call.enqueue(new Callback<JsonObject>() {

            // В случае если запрос выполнился успешно, то мы переходим в метод onResponse(...)
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    // Если в ответ нам пришел код 2xx, то отображаем содержимое запроса
                    JsonObject data = response.body().getAsJsonObject("Data").getAsJsonObject("General");
                    fillCurrencyInfo(
                            data.get("Name").getAsString(),
                            data.get("ImageUrl").getAsString(),
                            data.get("Symbol").getAsString()
                    );

                } else {
                    // Если пришел код ошибки, то обрабатываем её
                    Toast.makeText(CurrencyActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismiss();
            }

            // Если запрос не удалось выполнить, например, на телефоне отсутствует подключение к интернету
            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                // Скрываем progress bar
                loadingDialog.dismiss();
                Toast.makeText(CurrencyActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                Log.d("Error", t.getMessage());
            }
        });
    }


    private void fetchPrice(final String sourceCurrency, final String resultCurrency) {
        // Создаем экземпляр запроса со всем необходимыми настройками
        Call<JsonObject> call = cryptoPriceService.price(sourceCurrency, resultCurrency);

        // Выполняем запрос асинхронно
        call.enqueue(new Callback<JsonObject>() {

            // В случае если запрос выполнился успешно, то мы переходим в метод onResponse(...)
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    // Если в ответ нам пришел код 2xx, то отображаем содержимое запроса
                    String value = response.body().get(resultCurrency).getAsString();
                    String result = "1 ".concat(sourceCurrency).concat(" = ").concat(value).concat(" ").concat(resultCurrency);
                    price.setText(result);

                } else {
                    // Если пришел код ошибки, то обрабатываем её
                    Toast.makeText(CurrencyActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismiss();

            }

            // Если запрос не удалось выполнить, например, на телефоне отсутствует подключение к интернету
            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                // Скрываем progress bar
                loadingDialog.dismiss();
                Toast.makeText(CurrencyActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void fillCurrencyInfo(String name, String imageUrl, String symbol) {
        Picasso.with(this)
                .load(getString(R.string.base_image_url).concat(imageUrl))
                .into(icon);
        fullName.setText(name);
        shortName.setText(symbol);
    }

    private void fetchHistoricPrice(String currency, String resultCurrency) {
        // Создаем экземпляр запроса со всем необходимыми настройками
        Call<CryptoPriceBox> call = cryptoPriceService.historyPrice(currency, 15, resultCurrency);

        // Выполняем запрос асинхронно
        call.enqueue(new Callback<CryptoPriceBox>() {

            // В случае если запрос выполнился успешно, то мы переходим в метод onResponse(...)
            @Override
            public void onResponse(@NonNull Call<CryptoPriceBox> call, @NonNull Response<CryptoPriceBox> response) {
                if (response.isSuccessful()) {
                    // Если в ответ нам пришел код 2xx, то отображаем содержимое запроса
                    setData(response.body());

                } else {
                    // Если пришел код ошибки, то обрабатываем её
                    Toast.makeText(CurrencyActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                }

            }

            // Если запрос не удалось выполнить, например, на телефоне отсутствует подключение к интернету
            @Override
            public void onFailure(@NonNull Call<CryptoPriceBox> call, @NonNull Throwable t) {
                // Скрываем progress bar

                Toast.makeText(CurrencyActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                Log.d("Error", t.getMessage());
            }
        });
    }


    private void prepareChart() {
        priceChart.getDescription().setEnabled(false);


        // set an alternative background color
        priceChart.setBackgroundColor(Color.WHITE);

        XAxis xAxis = priceChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP_INSIDE);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(true);

        YAxis leftAxis = priceChart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        leftAxis.setDrawGridLines(false);
        leftAxis.setGranularityEnabled(true);

        YAxis rightAxis = priceChart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void setData(@NonNull CryptoPriceBox cryptoPriceBox) {
        ArrayList<CurrencyPrice> prices = cryptoPriceBox.getData();
        ArrayList<Entry> values = new ArrayList<>();

        Collections.sort(prices, new Comparator<CurrencyPrice>() {
            public int compare(CurrencyPrice o1, CurrencyPrice o2) {
                if (o1.getTime() > o2.getTime()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        // increment by 1 hour
        for (CurrencyPrice currencyPrice : prices) {
            values.add(new Entry(
                    currencyPrice.getTime() - Calendar.getInstance().get(Calendar.YEAR),
                    new Float(currencyPrice.getClose())
            ));
        }
        // create a dataset and give it a typen
        LineDataSet set1 = new LineDataSet(values, "price");
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
        set1.setColor(ColorTemplate.getHoloBlue());
        set1.setValueTextColor(ColorTemplate.getHoloBlue());
        set1.setDrawCircleHole(true);

        set1.setDrawFilled(true);


        // create a data object with the datasets
        LineData data = new LineData(set1);
        data.setDrawValues(true);
        // set data
        priceChart.setData(data);
        priceChart.invalidate();

        Legend l = priceChart.getLegend();
        l.setEnabled(false);
    }
}
