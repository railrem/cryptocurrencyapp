package railrem.bitcoinapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import railrem.bitcoinapp.R;
import railrem.bitcoinapp.api.cryptocompare.main.CryptoCurrencyService;
import railrem.bitcoinapp.api.cryptocompare.main.MainApiFactory;
import railrem.bitcoinapp.models.CryptoCurrency;
import railrem.bitcoinapp.models.CryptoCurrencyBox;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoinsActivity extends AppCompatActivity {

    @BindView(R.id.coins)
    public RecyclerView coinsList;

    private CryptoCurrencyService cryptoCurrencyService;

    private LinearLayoutManager mLinearLayoutManager;

    private ProgressDialog loadingDialog;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_coins);
        ButterKnife.bind(this);
        initViews();
        loadingDialog = ProgressDialog.show(this, "", getString(R.string.network_loading), true);

        // Создаем сервис через который будет выполняться запрос
        cryptoCurrencyService = MainApiFactory.getRetrofitInstance().create(CryptoCurrencyService.class);
        fetchCoins();
    }

    private void initViews() {
        mLinearLayoutManager = new LinearLayoutManager(this);
        coinsList.setLayoutManager(mLinearLayoutManager);
    }

    private void fetchCoins() {
        // Создаем экземпляр запроса со всем необходимыми настройками
        Call<CryptoCurrencyBox> call = cryptoCurrencyService.getCoinList();

        // Отображаем progress bar
        loadingDialog.show();

        // Выполняем запрос асинхронно
        call.enqueue(new Callback<CryptoCurrencyBox>() {

            // В случае если запрос выполнился успешно, то мы переходим в метод onResponse(...)
            @Override
            public void onResponse(@NonNull Call<CryptoCurrencyBox> call, @NonNull Response<CryptoCurrencyBox> response) {
                if (response.isSuccessful()) {
                    // Если в ответ нам пришел код 2xx, то отображаем содержимое запроса
                    fillCoins(response.body());

                } else {
                    // Если пришел код ошибки, то обрабатываем её
                    Toast.makeText(CoinsActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                }

                // Скрываем progress bar
                loadingDialog.dismiss();
            }

            // Если запрос не удалось выполнить, например, на телефоне отсутствует подключение к интернету
            @Override
            public void onFailure(@NonNull Call<CryptoCurrencyBox> call, @NonNull Throwable t) {
                // Скрываем progress bar
                loadingDialog.dismiss();

                Toast.makeText(CoinsActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                Log.d("Error", t.getMessage());
            }
        });
    }


    private void fillCoins(@NonNull CryptoCurrencyBox box) {
        ArrayList<CryptoCurrency> coins = new ArrayList<>(box.getData().values());
        CryptoCurrencyAdapter cryptoCurrencyAdapter = new CryptoCurrencyAdapter(coins, box.getBaseImageUrl(), this);
        coinsList.setAdapter(cryptoCurrencyAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(coinsList.getContext(),
                mLinearLayoutManager.getOrientation());
        coinsList.addItemDecoration(dividerItemDecoration);
    }


    public class CryptoCurrencyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        public TextView name;

        @BindView(R.id.shortcut)
        public TextView shortcut;

        @BindView(R.id.icon)
        public ImageView icon;

        public Context ctx;

        private Long id;

        public CryptoCurrencyViewHolder(View view,Context ctx) {
            super(view);
            ButterKnife.bind(this, view);
            this.ctx = ctx;
        }

        @OnClick(R.id.btnViewPrice)
        public void viewCurrency() {
            Intent intent = new Intent(ctx, CurrencyActivity.class);
            intent.putExtra(CurrencyActivity.EXTRA_CRYPTO_NAME, shortcut.getText().toString());
            intent.putExtra(CurrencyActivity.EXTRA_CRYPTO_ID, id.toString());
            startActivity(intent);
        }
    }

    public class CryptoCurrencyAdapter extends RecyclerView.Adapter<CryptoCurrencyViewHolder> {
        private List<CryptoCurrency> models;
        public String baseImageUrl;
        private Context ctx;

        public CryptoCurrencyAdapter(List<CryptoCurrency> myDataset, String baseImageUrl, Context ctx) {
            models = myDataset.subList(0, 30);
            this.baseImageUrl = baseImageUrl;
            this.ctx = ctx;
        }

        @Override
        public int getItemCount() {
            return models.size();
        }

        @Override
        public void onBindViewHolder(CryptoCurrencyViewHolder holder, int position) {
            CryptoCurrency cryptoCurrency = models.get(position);
            Picasso.with(ctx)
                    .load(this.baseImageUrl.concat(cryptoCurrency.getImageUrl()))
                    .into(holder.icon);
            holder.name.setText(cryptoCurrency.getCoinName());
            holder.shortcut.setText(cryptoCurrency.getName());
            holder.id =cryptoCurrency.getId();
        }

        @Override
        public CryptoCurrencyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflate = LayoutInflater.from(parent.getContext());
            View view = layoutInflate.inflate(R.layout.coins_item, parent, false);
            return new CryptoCurrencyViewHolder(view,ctx);
        }

    }
}
